﻿namespace AFSInterview
{
    using UnityEngine;

    public class Bullet : Projectile
    {
        [SerializeField] protected float speed;

        private void Update()
        {
            if (targetObject != null)
            {
                direction = (targetObject.transform.position - transform.position).normalized;
            }

            transform.position += direction * speed * Time.deltaTime;

            CheckTargetObjectIsInRange();
        }
    }
}