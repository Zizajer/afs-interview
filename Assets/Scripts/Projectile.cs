﻿namespace AFSInterview
{
    using UnityEngine;
    using System.Collections;

    public class Projectile : MonoBehaviour
    {
        protected GameObject targetObject;
        protected Vector3 direction;

        public virtual void Initialize(GameObject target)
        {
            targetObject = target;
        }

        protected void OnBecameInvisible()
        {
            Destroy(gameObject);
        }

        protected void CheckTargetObjectIsInRange()
        {
            if (targetObject != null)
            {
                if ((transform.position - targetObject.transform.position).magnitude <= 0.2f)
                {
                    Destroy(gameObject);
                    Destroy(targetObject);
                }
            }
        }
    }
}
