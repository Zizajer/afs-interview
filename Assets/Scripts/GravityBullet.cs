﻿namespace AFSInterview
{
    using UnityEngine;
    using System.Collections;

    public class GravityBullet : Projectile
    {
        [SerializeField]
        private float flyAngle = 45.0f;
        [SerializeField]
        private float gravity = 9.8f;

        public override void Initialize(GameObject target)
        {
            base.Initialize(target);
            StartCoroutine(SimulateProjectile());
        }

        IEnumerator SimulateProjectile()
        {
            Vector3 targetPosition = targetObject.transform.position;

            float distanceToTarget = (transform.position - targetPosition).magnitude;

            float bulletVelocity = distanceToTarget / (Mathf.Sin(2 * flyAngle * Mathf.Deg2Rad) / gravity);

            float verticalVolocity = Mathf.Sqrt(bulletVelocity) * Mathf.Cos(flyAngle * Mathf.Deg2Rad);
            float horizontalVelocity = Mathf.Sqrt(bulletVelocity) * Mathf.Sin(flyAngle * Mathf.Deg2Rad);

            float flightDuration = distanceToTarget / verticalVolocity;

            transform.rotation = Quaternion.LookRotation(targetPosition - transform.position);

            float elapsedTime = 0;

            while (elapsedTime < flightDuration)
            {
                transform.Translate(0, (horizontalVelocity - (gravity * elapsedTime)) * Time.deltaTime, verticalVolocity * Time.deltaTime);

                elapsedTime += Time.deltaTime;

                CheckTargetObjectIsInRange();

                yield return null;
            }

            Destroy(gameObject);
        }
    }
}
