﻿namespace AFSInterview
{
    using System.Collections.Generic;
    using TMPro;
    using UnityEngine;

    public class GameplayManager : MonoBehaviour
    {
        [Header("Prefabs")] 
        [SerializeField] private GameObject enemyPrefab;
        [SerializeField] private GameObject towerPrefab;
        [SerializeField] private GameObject burstTowerPrefab;

        [Header("Settings")] 
        [SerializeField] private Vector2 boundsMin;
        [SerializeField] private Vector2 boundsMax;
        [SerializeField] private float enemySpawnRate;

        [Header("UI")] 
        [SerializeField] private GameObject enemiesCountText;
        [SerializeField] private GameObject scoreText;
        
        private List<Enemy> enemies;
        private float enemySpawnTimer;
        private int score;
        private Vector3 spawnPosition;
        private Ray cameraRay;

        private void Awake()
        {
            enemies = new List<Enemy>();
        }

        private void Update()
        {
            enemySpawnTimer -= Time.deltaTime;

            if (enemySpawnTimer <= 0f)
            {
                SpawnEnemy();
                enemySpawnTimer = enemySpawnRate;
            }

            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
            {
                cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(cameraRay, out var hit, LayerMask.GetMask("Ground")))
                {
                    spawnPosition = hit.point;
                    spawnPosition.y = towerPrefab.transform.position.y;

                    if (Input.GetMouseButtonDown(0))
                    {
                        SpawnSimpleTower(spawnPosition);
                    }

                    if (Input.GetMouseButtonDown(1))
                    {
                        SpawnQuickBurstTower(spawnPosition);
                    }
                }
            }
        }

        private void SpawnEnemy()
        {
            var position = new Vector3(Random.Range(boundsMin.x, boundsMax.x), enemyPrefab.transform.position.y, Random.Range(boundsMin.y, boundsMax.y));
            
            var enemy = Instantiate(enemyPrefab, position, Quaternion.identity).GetComponent<Enemy>();
            enemy.OnEnemyDied += Enemy_OnEnemyDied;
            enemy.Initialize(boundsMin, boundsMax);

            enemies.Add(enemy);
            UpdateEnemyText();
        }

        private void Enemy_OnEnemyDied(Enemy enemy)
        {
            enemies.Remove(enemy);
            score++;
            UpdateScoreText();
            UpdateEnemyText();
        }

        private void SpawnSimpleTower(Vector3 position)
        {
            var tower = Instantiate(towerPrefab, position, Quaternion.identity).GetComponent<SimpleTower>();
            tower.Initialize(enemies);
        }

        private void SpawnQuickBurstTower(Vector3 position)
        {
            var tower = Instantiate(burstTowerPrefab, position, Quaternion.identity).GetComponent<SimpleTower>();
            tower.Initialize(enemies);
        }

        private void UpdateScoreText()
        {
            if (scoreText != null)
            {
                scoreText.GetComponent<TextMeshProUGUI>().text = "Score: " + score;
            }
        }

        private void UpdateEnemyText()
        {
            if (enemiesCountText != null)
            {
                enemiesCountText.GetComponent<TextMeshProUGUI>().text = "Enemies: " + enemies.Count;
            }
        }
    }
}