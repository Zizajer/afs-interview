﻿namespace AFSInterview
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;

    public class QuickBurstTower : SimpleTower
    {
        [SerializeField] private int burstBulletsAmount;
        [SerializeField] private float shootInterval;

        public override void Initialize(IReadOnlyList<Enemy> enemies)
        {
            base.Initialize(enemies);

            if (burstBulletsAmount > 0)
            {
                fireTimer += shootInterval * burstBulletsAmount - 1;
            }
        }

        protected override void Shoot()
        {
            for(int i = 0; i < burstBulletsAmount; i++)
            {
                Invoke("CreateBullet", shootInterval * i);
            }
        }
    }
}
